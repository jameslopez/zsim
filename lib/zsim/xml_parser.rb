# frozen_string_literal: true

require 'nokogiri/xml/builder'
require 'fileutils'
require 'date'

module XmlParser
  def self.parse(hash)
    xml = parse_xml(hash)
    return unless current_user?(xml)

    file = filename(hash)
    File.write(file, xml) unless File.exist?(file)
    yield(xml, callout_path(hash), 'text/xml')
  end

  def self.parse_xml(hash)
    hash[:event_context].each do |object|
      param = Nokogiri::XML(object[0].to_s).children.first.name
      next if param != "send_xml"

      return object[1]
    end; nil
  end

  def self.filename(hash)
    dir = "./callouts/#{Date.parse(hash[:create_time])}"
    context = hash[:event_context][:'<account.number>'] || hash[:event_context][:'<process.id>']
    name = "#{hash[:create_time]}-#{context}"
    FileUtils.mkdir_p(dir)

    "#{dir}/#{name}.xml"
  end

  def self.callout_path(hash)
    URI.parse(hash[:request_url])&.path
  end

  def self.current_user?(xml)
    doc = Nokogiri::XML(xml)
    user_id = doc.xpath('//parameter[@name="creator_id"]').text

    user_id == ENV['ZUORA_USER_ID']
  end

  private_class_method :parse_xml
  private_class_method :filename
end
