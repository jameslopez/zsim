# frozen_string_literal: true

require 'json'
module JsonParser
  extend self

  def parse(hash)
    event_context = hash[:event_context]
    return unless current_user?(event_context)

    json = event_context[:'<send_xml>']
    parsed = JSON.parse(json)
    json = parsed.except("development").to_json if parsed.key?("development")

    write_to_file(event_context, json)

    yield(json, callout_path(hash), 'application/json')
  end

  private

  def current_user?(event_context)
    user_id = event_context[:'<user.id>']

    user_id == ENV['ZUORA_USER_ID']
  end

  def write_to_file(event_context, json)
    file = file_path(event_context)

    File.write(file, json) unless File.exist?(file)
  end

  def file_path(event_context)
    dir = create_dir(event_context)
    name = filename(event_context)

    "#{dir}/#{name}.json"
  end

  def create_dir(event_context)
    date = event_context[:'<event.date>']
    dir = "./callouts/#{date}"
    FileUtils.mkdir_p(dir)

    dir
  end

  def filename(event_context)
    context = event_context[:'<account.account_number>'] || event_context[:'<process.id>']
    event_timestamp = event_context[:'<event.timestamp>']
    formatted_time = Time.parse(event_timestamp).strftime("%Y-%m-%dT%H:%M:%S")

    "#{formatted_time}-#{context}"
  end

  def callout_path(hash)
    path = URI.parse(hash[:request_url])&.path

    path.sub('development/', '')
  end
end
