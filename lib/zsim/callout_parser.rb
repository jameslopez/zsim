# frozen_string_literal: true

module CalloutParser
  def self.parse_and_save(hashes, &block)
    [hashes].flatten.compact.each do |hash|
      next if Config.ignore_errored_callouts && hash[:response_code].to_s.start_with?('5')
      next unless hash[:request_url] && Regexp.new(Config.request_url_regex).match?(hash[:request_url])
      next unless hash[:event_category] && Regexp.new(Config.event_category_regex).match?(hash[:event_category])
      next unless hash[:attempted_num] == 1
      next if hash[:event_context][:'<sold_to_contact.work_email>'] &&
        !Regexp.new(Config.sold_to_contact_email_regex).match?((hash[:event_context][:'<sold_to_contact.work_email>']))

      content_type = hash[:event_context][:'<content_type>']

      case content_type.downcase
      when 'text_xml'
        XmlParser.parse(hash, &block)
      when 'application_json'
        JsonParser.parse(hash, &block)
      end
    end
  end
end
