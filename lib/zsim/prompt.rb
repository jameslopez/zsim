# frozen_string_literal: true

require 'pastel'
require 'tty-font'
require 'tty-prompt'

module Zsim
  class Prompt
    INITIAL_OPTIONS = [
      'Listen/Replay new callouts',
      'Replay callouts for last X hours',
      { name: 'Replay last', disabled: '(Not Implemented)' },
      'Show last',
      'Replay callouts for Zuora Order',
      { name: 'Configuration', disabled: '(Not Implemented)' },
      'Exit'
    ].freeze

    attr_reader :options, :prompt

    def initialize
      refresh

      @options = INITIAL_OPTIONS.dup
      @prompt = TTY::Prompt.new
    end

    def execute(show_prompt: true)
      if show_prompt
        run_choices
      else
        run_default_choice
      end
    end

    private

    def refresh
      system('clear')

      font = TTY::Font.new(:starwars)
      pastel = Pastel.new

      puts pastel.yellow(font.write('ZSim'))
      puts pastel.bold(" Version #{Zsim::VERSION} - Experimental\n")
    end

    def run_choices
      choice = prompt.select('', options, cycle: true, filter: true, echo: false)

      refresh

      process_choice(choice)

      run_choices
    end

    def callout
      @callout ||= Callout.new
    end

    def process_choice(choice)
      case choice
      when 'Listen/Replay new callouts'
        choice_listen
      when 'Replay callouts for last X hours'
        choice_stop_listen
        choice_replay_last_x_hours
      when 'Stop listening/Replaying callouts'
        choice_stop_listen
      when 'Replay callouts for Zuora Order'
        choice_stop_listen
        choice_filter_by_order
      when 'Show last'
        choice_show_last
      when 'Exit'
        exit(0)
      end
    end

    def choice_listen
      callout.listen_to_new

      options.delete('Listen/Replay new callouts')
      options.prepend('Stop listening/Replaying callouts')

      prompt.warn('Listening')
    end

    def choice_replay_last_x_hours
      input = prompt.ask("Enter number of hours (Press enter to return):") do |question|
        question.in("1-72")

        question.validate ->(input) { input =~ /\A\d+\Z/ }
        question.messages[:valid?] = "Invalid value. Please enter a number"
      end

      hours = input.to_i

      prompt.say("Replaying callouts from #{input} hour(s) ago", color: :bright_blue)

      callout.replay_since(hours: hours)

      prompt.ok("Callouts retrieval for last #{input} hour(s) complete")
    end

    def choice_filter_by_order
      order_number = prompt.ask("Enter Order number (Press enter to return):")
      prompt.say("Retrieving Order details", color: :bright_blue)

      result = order_details(order_number)

      prompt.say("Order details retrieved successfully. Beginning to replay callouts", color: :bright_blue)

      # no conversion is required as order date in API response is returned in Zuora Time
      start_time = Time.new(result[:order_date].year, result[:order_date].month, result[:order_date].day)
      callout.filter_by(start_time: start_time, identifier: result[:identifier])

      prompt.ok("Callouts replay for Order number '#{order_number}' complete")
    rescue ::IronBank::NotFoundError, ::IronBank::UnprocessableEntityError, ::IronBank::BadGatewayError => e
      prompt.error(e)
    end

    def choice_stop_listen
      callout.stop

      options.delete('Stop listening/Replaying callouts')
      options.prepend('Listen/Replay new callouts') unless options.include?('Listen/Replay new callouts')

      prompt.warn('Stopped')
    end

    def choice_show_last
      system('clear')

      last_file = Dir.glob('./callouts/**/*').max_by { |f| File.ctime(f) }
      if last_file
        if last_file.end_with?('.json')
          puts File.read(last_file)
        else
          system('xmllint', last_file)
        end
      else
        prompt.warn('No previous callout files found')
      end

      prompt.keypress('Press any key to continue')

      refresh
    end

    def run_default_choice
      callout.listen_to_new

      prompt.warn('Listening')
      prompt.keypress('Press any key to stop')
    end

    def order_details(order_number)
      query = "select Id, orderDate from Orders where orderNumber='#{order_number}'"
      result = IronBank::Query.post({ query: query })

      query_result = IronBank::Query.get({ job_id: result[:data][:id] })
      order_date = Date.parse(query_result["orderDate"])

      { order_date: order_date, identifier: query_result["Id"] }
    end
  end
end
