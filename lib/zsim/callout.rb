# frozen_string_literal: true

class Callout
  REFRESH_INTERVAL = 60
  TO_SECONDS = 60 * 60

  def listen_to_new
    @next_start = Concurrent::MVar.new(ZuoraTime.now)

    @task = Concurrent::TimerTask.new(
      execution_interval: Config.refresh * REFRESH_INTERVAL, timeout_interval: REFRESH_INTERVAL) do
      start_time = @next_start.take
      end_time = start_time + REFRESH_INTERVAL

      retrieve_and_save(start_time: start_time, end_time: end_time)

      @next_start.put(end_time)
    end

    @task.add_observer(CalloutObserver.new)
    @task.execute
  end

  def replay_since(hours:)
    number_of_seconds = hours * TO_SECONDS
    start_time = ZuoraTime.now - number_of_seconds

    retrieve_and_save(start_time: start_time)
  end

  def filter_by(start_time:, identifier:, end_time: nil)
    end_time ||= start_time + (IronBank::Actions::NotificationCallout::MAX_HOURS * TO_SECONDS)

    retrieve_and_save(start_time: start_time, end_time: end_time, identifier: identifier)
  end

  def stop
    return unless @task

    @task.shutdown
    sleep 1 while @task&.running?
    @task = nil
  end

  private

  def retrieve_and_save(start_time:, end_time: nil, identifier: nil)
    notifications = IronBank::Actions::NotificationCallout.new(
      start_time: start_time, end_time: end_time, identifier: identifier
    )

    CalloutParser.parse_and_save(notifications.call) do |body, path, type|
      Export.send_callout(body: body, callout_path: path, type: type)
    end
  end
end
