# frozen_string_literal: true

require 'httparty'

module Export
  def self.send_callout(body:, callout_path:, type:)
    HTTParty.post(
      "#{ENV.fetch('CUSTOMERS_DOT_CALLOUT_URL', 'http://localhost:5000')}#{callout_path}",
      body: body,
      headers: { 'Content-type' => type },
      basic_auth: { username: ENV.fetch('ZUORA_CALLBACK_USERNAME', nil),
                    password: ENV.fetch('ZUORA_CALLBACK_PASSWORD', nil) },
      timeout: Config.customers_dot_timeout
    )
  end
end
