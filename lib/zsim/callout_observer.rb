# frozen_string_literal: true

class CalloutObserver
  def initialize
    @prompt = TTY::Prompt.new

    super
  end

  def update(time, _result, ex)
    if ex.is_a?(Concurrent::TimeoutError)
      @prompt.error("\n(#{time}) Execution timed out\n")
    elsif ex
      @prompt.error("\n#{time}) Execution failed with error #{ex}\n")
      @prompt.error("\n#{time}) #{ex.backtrace}")
    end
  end
end
