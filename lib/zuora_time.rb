# frozen_string_literal: true

require 'tzinfo'

class ZuoraTime
  def self.now
    Time.now.getlocal(TZInfo::Timezone.get('US/Pacific'))
  end
end
