# frozen_string_literal: true

require 'yaml'

class Config
  FILE = './config.yml'

  def self.method_missing(name, *args)
    if name[-1] == '='
      config[name.to_s.chop] = args[0]
      save_config
    else
      config[name.to_s]
    end
  end

  def self.respond_to_missing?(name, *)
    sanitized_name = name[-1] == '=' ? name.chop : name
    config.key?(sanitized_name.to_s) || super
  end

  def self.config
    YAML.load_file(FILE)
  end

  def save_config
    File.write(FILE, config.to_yaml)
  end
end
