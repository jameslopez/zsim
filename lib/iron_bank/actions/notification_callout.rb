# frozen_string_literal: true

require 'date'
require 'nokogiri/xml/builder'
require 'iron_bank'

module IronBank
  module Actions
    # Use the subscribe call to bundle information required to create at least
    # one new subscription
    # https://www.zuora.com/developer/api-reference/#operation/Action_POSTsubscribe
    # (DateTime.now - 3).strftime('%FT%T')
    class NotificationCallout < Action
      MAX_PAGE_SIZE = 40
      MAX_HOURS = 72

      public_class_method :new

      def initialize(start_time:, end_time: nil, identifier: nil)
        @start_time = start_time
        @end_time = end_time || ZuoraTime.now
        @identifier = identifier

        super
      end

      def call
        @body = IronBank.client.connection.get(endpoint, params).body

        all_callouts = []

        loop do
          raise ::IronBank::UnprocessableEntityError, errors unless success?

          current_page_callouts = IronBank::Object.new(body).deep_underscore[:callout_histories]
          all_callouts << current_page_callouts

          # retrieve the next page callouts, only if nextPage present
          next_page = body['nextPage']
          break unless next_page

          @body = IronBank.client.connection.get(next_page).body
        end

        all_callouts.flatten
      end

      private

      def params
        params = {
          startTime: @start_time.strftime('%FT%T'),
          endTime: @end_time.strftime('%FT%T'),
          pageSize: MAX_PAGE_SIZE
        }
        params[:objectId] = @identifier if @identifier

        params
      end

      def success?
        @body[:success] || @body['success']
      end

      def errors
        { errors: @body['reasons']&.first&.fetch('message') || @body[:errors] }
      end

      def endpoint
        'v1/notification-history/callout'
      end
    end
  end
end
