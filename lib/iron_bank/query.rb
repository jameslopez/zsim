# frozen_string_literal: true

require 'iron_bank'

module IronBank
  class Query
    private_class_method :new

    def self.post(attrs)
      new(attrs).post
    end

    def self.get(attrs)
      new(attrs).get
    end

    def post
      @body = IronBank.client.connection.post(endpoint, post_params).body

      raise ::IronBank::UnprocessableEntityError, errors unless success?

      IronBank::Object.new(body).deep_underscore
    end

    def get
      max_attempts = 3
      result = 1.upto(max_attempts) do
        @body = IronBank.client.connection.get("#{endpoint}/#{attrs[:job_id]}").body

        raise ::IronBank::UnprocessableEntityError, errors unless success?

        response = IronBank::Object.new(body).deep_underscore

        if response[:data][:query_status] == "completed"
          data_file = response[:data][:data_file]
          content = HTTParty.get(data_file)

          raise ::IronBank::NotFoundError, "Order not found" if content.parsed_response.nil?

          break JSON.parse(content)
        else
          sleep 5
        end
      end

      return result if result.respond_to?(:key) && result.key?("Id")

      raise ::IronBank::BadGatewayError, "Request timed out. Please try again."
    end

    private

    attr_reader :attrs, :body

    def initialize(attrs)
      @attrs = attrs
    end

    def endpoint
      "query/jobs"
    end

    def post_params
      {
        compression: 'NONE',
        output: { target: 'S3' },
        outputFormat: 'JSON',
        query: attrs[:query]
      }
    end
  end
end
