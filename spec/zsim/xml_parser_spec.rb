# frozen_string_literal: true

require 'rspec'

describe XmlParser do
  let(:filename) { './callouts/2021-09-16/2021-09-16T05:54:31-123.xml' }
  let(:callback_path) { '/zuora_callback/order_processed' }

  let(:hash) do
    { create_time: '2021-09-16T05:54:31',
      event_context: {
        '<account.number>': '123', '<sold_to_contact.work_email>': 'test@example.com',
        '<send_xml>': xml, '<content_type>': 'TEXT_XML'
      },
      request_url: "http://localhost:5000#{callback_path}",
      event_category: 'test' }
  end

  let(:xml) { file_fixture('sample_callout.xml').read }
  let(:zuora_user_id_config) { '6b9dd7dc016ba9' }

  before do
    allow(ENV).to receive(:[]).with("ZUORA_USER_ID").and_return(zuora_user_id_config)
  end

  after do
    FileUtils.rm_f(filename)
  end

  it 'parses Zuora callout hashes' do
    allow(FileUtils).to receive(:mkdir_p)
    expect(File).to receive(:write).with(filename, xml)

    described_class.parse(hash) do |response, path, type|
      expect(response).to eq(xml)
      expect(path).to eq(callback_path)
      expect(type).to eq('text/xml')
    end
  end

  context 'when callout response is not the current user' do
    let(:zuora_user_id_config) { 'another-user' }

    it do
      described_class.parse(hash) do |response, path|
        expect(response).to be_nil
        expect(path).to be_nil
      end
    end
  end

  describe 'validations' do
    context 'when work email is not present' do
      let(:hash) do
        { create_time: '2021-09-16T05:54:31',
          event_context: {
            '<account.number>': '123',
            '<send_xml>': xml
          },
          request_url: "http://localhost:5000#{callback_path}",
          event_category: 'test' }
      end

      it 'writes callout response to file' do
        allow(FileUtils).to receive(:mkdir_p)
        expect(File).to receive(:write)

        described_class.parse(hash) do |response, path, type|
          expect(response).to eq(xml)
          expect(path).to eq(callback_path)
          expect(type).to eq('text/xml')
        end
      end
    end
  end
end
