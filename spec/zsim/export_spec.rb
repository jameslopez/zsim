# frozen_string_literal: true

require 'rspec'

describe Export do
  let(:path) { '/zuora_callback/subscription_update' }

  shared_examples 'sends request to CustomersDot with expected content type' do
    it "send request to CustomersDot" do
      expect(HTTParty).to receive(:post).with("http://localhost:5000#{path}",
        { basic_auth: { password: nil, username: nil },
          body: body,
          headers: { 'Content-type' => content_type },
          timeout: 10 })

      described_class.send_callout(body: body, callout_path: path, type: content_type)
    end
  end

  context 'when content type is text/xml' do
    let(:content_type) { 'text/xml' }
    let(:body) { '<xml></xml>' }

    include_examples 'sends request to CustomersDot with expected content type'
  end

  context 'when content type is application/json' do
    let(:content_type) { 'application/json' }
    let(:body) do
      {
        AccountNumber: "A01041087", OrderDate: "2024-01-23",
        OrderId: "7e74018d34f73bfb9096e87d11d90000", OrderNumber: "O-00552631"
      }
    end

    include_examples 'sends request to CustomersDot with expected content type'
  end
end
