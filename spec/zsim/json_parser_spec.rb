# frozen_string_literal: true

require 'rspec'

describe JsonParser do
  let(:filename) { './callouts/2021-09-16/2021-09-16T05:54:31-123.json' }
  let(:user_id) { "1234" }
  let(:request_url) { "http://localhost:5000/development/zuora_callback/order_processed" }

  let(:received_content) do
    file_fixture('subscription_update_callout.json').read
  end

  let(:hash) do
    { event_context: {
        '<account.account_number>': '123', '<sold_to_contact.work_email>': 'test@example.com',
        '<send_xml>': received_content, '<content_type>': 'APPLICATION_JSON',
        '<event.date>': '2021-09-16',
        '<event.timestamp>': '2021-09-16T05:54:31',
        '<user.id>': user_id
      },
      request_url: request_url,
      event_category: 'test' }
  end

  before do
    allow(ENV).to receive(:[]).with("ZUORA_USER_ID").and_return(user_id)
  end

  after do
    FileUtils.rm_f(filename)
  end

  shared_examples 'parses Zuora callout hashes' do
    it 'parses Zuora callout hashes' do
      expected_content = JSON.parse(received_content).except("development").to_json
      allow(FileUtils).to receive(:mkdir_p)
      expect(File).to receive(:write).with(filename, expected_content)

      described_class.parse(hash) do |response, path, type|
        expect(response).to eq(expected_content)
        expect(path).to eq('/zuora_callback/order_processed')
        expect(type).to eq('application/json')
      end
    end
  end

  context 'when request url includes /development' do
    it_behaves_like 'parses Zuora callout hashes'
  end

  context 'when request url includes /zuora_callback' do
    let(:request_url) { "http://localhost:5000/zuora_callback/order_processed" }

    it_behaves_like 'parses Zuora callout hashes'
  end

  context 'when callout response is not the current user' do
    before do
      allow(ENV).to receive(:[]).with("ZUORA_USER_ID").and_return('another-user-id')
    end

    it 'does not parse the callout' do
      described_class.parse(hash) do |response, path|
        expect(response).to be_nil
        expect(path).to be_nil
      end
    end
  end
end
