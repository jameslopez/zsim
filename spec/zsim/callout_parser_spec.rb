# frozen_string_literal: true

require 'rspec'

describe CalloutParser do
  let(:hashes) do
    [
      {
        event_context: {
          '<sold_to_contact.work_email>': 'test@example.com',
          '<content_type>': content_type, '<send_xml>': content
        },
        request_url: "http://localhost:5000/zuora_callback/order_processed",
        event_category: 'test', attempted_num: attempted_num,
        response_code: response_code
      }
    ]
  end

  let(:content) { file_fixture('sample_callout.xml').read }
  let(:attempted_num) { 1 }
  let(:content_type) { 'TEXT_XML' }
  let(:response_code) { 400 }

  context 'when invalid' do
    shared_examples 'does not invoke parser' do
      specify do
        expect(XmlParser).not_to receive(:parse)

        described_class.parse_and_save(hashes)
      end
    end

    context 'when response code is invalid' do
      let(:response_code) { 500 }

      before do
        allow(Config).to receive(:ignore_errored_callouts).and_return(true)
      end

      it_behaves_like 'does not invoke parser'
    end

    context 'when request url does not match config' do
      before do
        allow(Config).to receive(:request_url_regex).and_return(/test/)
      end

      it_behaves_like 'does not invoke parser'
    end

    context 'when event category does not match config' do
      before do
        allow(Config).to receive(:event_category_regex).and_return(/invalid/)
      end

      it_behaves_like 'does not invoke parser'
    end

    context 'when callout response is already processed' do
      let(:attempted_num) { 2 }

      it_behaves_like 'does not invoke parser'
    end

    context 'when sold to contact email does not match config' do
      before do
        allow(Config).to receive(:sold_to_contact_email_regex).and_return(/invalid/)
      end

      it_behaves_like 'does not invoke parser'
    end
  end

  context 'when valid' do
    context 'when content type is XML' do
      specify do
        expect(XmlParser).to receive(:parse).with(hashes.first)

        described_class.parse_and_save(hashes)
      end
    end

    context 'when content type is JSON' do
      let(:content_type) { 'APPLICATION_JSON' }
      let(:content) { file_fixture('subscription_update_callout.json').read }

      specify do
        expect(JsonParser).to receive(:parse).with(hashes.first)

        described_class.parse_and_save(hashes)
      end
    end
  end
end
