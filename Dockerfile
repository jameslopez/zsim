ARG RUBY_VERSION=3.2.5
FROM ruby:$RUBY_VERSION-alpine3.19

# Nokogiri's build and runtime dependencies
RUN apk add --no-cache \
    xz-libs \
    build-base \
    libxml2-dev \
    libxslt-dev \
    tzdata && \
    # Clean up
    rm -rf /var/cache/apk/*

WORKDIR /home/zsim

COPY Gemfile Gemfile.lock ./

RUN bundle config set without "development test" && \
    bundle install --jobs=3 --retry=3

RUN adduser -D zsim
USER zsim
COPY --chown=zsim . ./

CMD ["./bin/zsim"]
